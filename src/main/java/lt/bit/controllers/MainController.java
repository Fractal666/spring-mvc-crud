/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package lt.bit.controllers;

import com.mycompany.springdata.Address;
import com.mycompany.springdata.Contacts;
import com.mycompany.springdata.People;
import java.math.BigDecimal;
import java.sql.Date;
import java.util.List;
import lt.bit.repositories.AddressRep;
import lt.bit.repositories.ContactRep;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationContext;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.servlet.ModelAndView;
import lt.bit.repositories.PeopleRep;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestParam;

/**
 *
 * @author Fractal
 */
@Controller
@RequestMapping
public class MainController {
    
    private static final Log log = LogFactory.getLog(MainController.class);
    
    @Autowired
    private AddressRep batonas;
    @Autowired
    private PeopleRep kepalas;
    @Autowired
    private ContactRep duona;
    @RequestMapping
    public ModelAndView hello(){
        System.out.println("Hello");
        ModelAndView mav = new ModelAndView("peopleList");
//        mav.addObject("list", kepalas.findByName("%as"));
        mav.addObject("list", kepalas.findAll());
       
        return mav;
    }
    
    @Transactional
    @RequestMapping(path="editPerson/{id}", method=RequestMethod.GET)
    public ModelAndView editPerson(@PathVariable("id") Integer id){
        ModelAndView mav = new ModelAndView("updatePerson");
        People foundppl = kepalas.getOne(id);
        foundppl.setAddressList(null);
        foundppl.setContactsList(null);
        
        mav.addObject("person", foundppl);
        
        return mav;
    }
    
    @Transactional
    @RequestMapping(path="savePeople/{id}", method=RequestMethod.POST)
    public String changePerson(@RequestParam(value="firstName", required=true) String firstName,
            @RequestParam(value="lastName", required=true) String lastName,
            @RequestParam(value="birthDate", required=true) String birthDate,
            @RequestParam(value="salary", required=true) BigDecimal salary,
            Model model, @PathVariable("id") Integer id){
        People foundPerson = kepalas.getOne(id);
        foundPerson.setAddressList(null);
        foundPerson.setContactsList(null);
        foundPerson.setFirstName(firstName);
        foundPerson.setLastName(lastName);
        foundPerson.setBirthDate(Date.valueOf(birthDate));
        foundPerson.setSalary(salary);
        kepalas.save(foundPerson);
        return "redirect:/";
    }
    
    @Transactional
    @RequestMapping(path="delete/{id}", method=RequestMethod.GET)
    public String deletePerson(Model model, @PathVariable("id") Integer id){
        People foundPerson = kepalas.getOne(id);
        foundPerson.setAddressList(null);
        foundPerson.setContactsList(null);
        
        kepalas.delete(foundPerson);
        return "redirect:/";
    }
    
    @Transactional
    @RequestMapping(path="addPerson", method=RequestMethod.POST)
    public String addPerson(@RequestParam(value="firstName", required=true) String firstName,
            @RequestParam(value="lastName", required=true) String lastName,
            @RequestParam(value="birthDate", required=true) String birthDate,
            @RequestParam(value="salary", required=true) BigDecimal salary,
            Model model){
        People foundPerson = new People();
        foundPerson.setFirstName(firstName);
        foundPerson.setLastName(lastName);
        foundPerson.setBirthDate(Date.valueOf(birthDate));
        foundPerson.setSalary(salary);
        kepalas.save(foundPerson);
        return "redirect:/";
    }
    
    @RequestMapping(path="Add", method=RequestMethod.GET)
    public ModelAndView AddPerson(){
        ModelAndView mav = new ModelAndView("Add");
        
        return mav;
    }
//    @Transactional
//    @RequestMapping(path="savePeople/{id}", method=RequestMethod.POST)
//    public ModelAndView changePerson(@RequestParam(value="firstName", required=true) String firstName,
//            @RequestParam(value="lastName", required=true) String lastName,
//            @RequestParam(value="birthDate", required=true) String birthDate,
//            @RequestParam(value="salary", required=true) BigDecimal salary,
//             @PathVariable("id") Integer id){
//        ModelAndView mav = new ModelAndView("redirect:/");
//        People foundPerson = kepalas.getOne(id);
//        foundPerson.setAddressList(null);
//        foundPerson.setContactsList(null);
//        foundPerson.setFirstName(firstName);
//        foundPerson.setLastName(lastName);
//        foundPerson.setBirthDate(Date.valueOf(birthDate));
//        foundPerson.setSalary(salary);
//        kepalas.save(foundPerson);
//        
//        return mav;
//    }
//    
//    @Transactional
//    @RequestMapping(path="delete/{id}", method=RequestMethod.GET)
//    public ModelAndView deletePerson(Model model, @PathVariable("id") Integer id){
//        ModelAndView mav = new ModelAndView("redirect:/");
//        People foundPerson = kepalas.getOne(id);
//        foundPerson.setAddressList(null);
//        foundPerson.setContactsList(null);
//        
//        kepalas.delete(foundPerson);
//        return mav;
//    }
    @RequestMapping(path="Addresses/{id}", method=RequestMethod.GET)
    public ModelAndView getAddress(@PathVariable("id") Integer id){
        ModelAndView mav = new ModelAndView("Address");
        People fp = kepalas.getOne(id);
        List<Address> pplAddress = batonas.findByPeopleId(fp);
        mav.addObject("list", pplAddress);
        
        return mav;
        
    }
    
    @RequestMapping(path="contacts/{id}", method=RequestMethod.GET)
    public ModelAndView getContacts(@PathVariable("id") Integer id){
        ModelAndView mav = new ModelAndView("Contact");
        People fp = kepalas.getOne(id);
        List<Contacts> pplAddress = duona.findByPeopleId(fp);
        mav.addObject("list", pplAddress);
        
        return mav;
        
    }
   
}
