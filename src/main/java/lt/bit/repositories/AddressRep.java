/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package lt.bit.repositories;

import com.mycompany.springdata.Address;
import com.mycompany.springdata.People;
import java.util.List;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

/**
 *
 * @author Fractal
 */
public interface AddressRep extends JpaRepository<Address, Integer>{
    
    @Query("select p from Address p where peopleId = :peopleid")
    List<Address> findByPeopleId(@Param("peopleid") People peopleid);
    
}
