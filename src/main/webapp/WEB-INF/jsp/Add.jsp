<%-- 
    Document   : Add
    Created on : Oct 25, 2018, 2:28:37 PM
    Author     : Fractal
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>JSP Page</title>
    </head>
    <body>
         <form action="addPerson" method="POST">
            <input type="text" name="firstName">
            <input type="text" name="lastName">
            <input type="text" name="birthDate">
            <input type="number" name="salary">
            <input type="submit" value="Save">
        </form>
    </body>
</html>
