<%-- 
    Document   : peopleList
    Created on : Oct 23, 2018, 5:11:25 PM
    Author     : Fractal
--%>

<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>JSP Page</title>
        <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/css/bootstrap.min.css" integrity="sha384-MCw98/SFnGE8fJT3GXwEOngsV7Zt27NXFoaoApmYm81iuXoPkFOJwJ8ERdknLPMO" crossorigin="anonymous">
    </head>

    <body>
        <a href="Add">AddNew</a>
        <table class="table table-success table-hover table-striped table-bordered">
            <thead class="thead-dark">
            <th>First Name</th>
            <th>Last Name</th>
            <th>Birth Date</th>
            <th>Salary</th>
            <th>Edit</th>
            <th>Delete</th>
        </thead> 
        <c:forEach var="People" items="${list}">
            <tr>
                <td>${People.firstName}</td>
                <td>${People.lastName}</td>
                <td>${People.birthDate}</td>
                <td>${People.salary}</td>
                <td><a href="Addresses/${People.id}">Address</a></td>
                <td><a href="contacts/${People.id}">Contacts</a></td>
                <td><a href="editPerson/${People.id}">Edit</a></td>
            <form action="delete/${People.id}" method="GET"> <td><input type="submit" value="delete"></td></form>




            </tr>
        </c:forEach>
    </table>
</body>
</html>
