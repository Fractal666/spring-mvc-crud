<%-- 
    Document   : updatePerson
    Created on : Oct 25, 2018, 12:48:28 PM
    Author     : Fractal
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>JSP Page</title>
    </head>
    <body>
        <form action="/SpringData/savePeople/${person.id}" method="POST">
            <input type="text" name="firstName" value="${person.firstName}">
            <input type="text" name="lastName" value="${person.lastName}">
            <input type="text" name="birthDate" value="${person.birthDate}">
            <input type="number" name="salary" value="${person.salary}">
            <input type="submit" value="Save">
        </form>
    </body>
</html>
